import * as mysql from "mysql"
import  config  from "./config"

const connection = mysql.createConnection(config)
    

export default connection